/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package calculadora_vladyslav_golovatyi_tsymbal;

import java.util.Scanner;

/**
 *
 * @author DAM113
 */
public class Calculadora_Vladyslav_Golovatyi_Tsymbal {

    static Scanner scanner = new Scanner(System.in);
    static int opcion = -1;
    static double numero1 = 0, numero2 = 0;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        while (opcion != 0) {
            //Try catch para evitar que el programa termine si hay un error
            try {
                System.out.println("Elige opción:\n" + ""
                        + "1.- Sumar\n"
                        + "2.- Restar\n"
                        + "3.- Multiplicar\n"
                        + "4.- Dividir\n"
                        + "0.- Salir");

                System.out.println("Selecciona una opción de 0 a 4");
                opcion = Integer.parseInt(scanner.nextLine());

                switch (opcion) {
                    case 1:
                        pideNumeros();
                        System.out.println(numero1 + " + " + numero2 + " = " + (numero1 + numero2));
                        break;
                    case 2:
                        pideNumeros();
                        System.out.println(numero1 + " - " + numero2 + " = " + (numero1 - numero2));
                        break;
                    case 3:
                        pideNumeros();
                        System.out.println(numero1 + " * " + numero2 + " = " + (numero1 * numero2));
                        break;
                    case 4:
                        pideNumeros();
                        System.out.println(numero1 + " / " + numero2 + " = " + (numero1 / numero2));
                        break;
                    case 5:
                        int total,
                         score;
                        float percentage;
                        Scanner inputNumScanner = new Scanner(System.in);

                        System.out.println("Ingresa el puntaje total, o máximo: ");
                        total = inputNumScanner.nextInt();

                        System.out.println("Ingresa el puntaje obtenido: ");
                        score = inputNumScanner.nextInt();

                        percentage = (score * 100 / total);

                        System.out.println("El porcentaje es = " + percentage + " %");
                        break;
                    case 0:
                        System.out.println("Saliendo...");
                        break;
                    default:
                        System.out.println("Opción no disponible");
                        break;
                }

                System.out.println("\n");

            } catch (Exception e) {
                System.out.println("Error!");
            }
        }

    }

    //Método para recoger las variables de entrada
    public static void pideNumeros() {
        System.out.println("Introduce el primer número:");
        numero1 = Integer.parseInt(scanner.nextLine());

        System.out.println("Introduce el segundo número:");
        numero2 = Integer.parseInt(scanner.nextLine());
    }
}
